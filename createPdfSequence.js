var fs = require('fs')
var asyncLoop = require('node-async-loop');
var conversion = require('phantom-html-to-pdf')();
const sequenceData = require('./pdfSequenceData.json').sequence;
// Creates a PDF for each object in an array
// Each page is rendered in an HTML browser then exported to a PDF
// This means you can link external CSS and JS files
// The object in the array is passed into a hidden div, which can then be accessed on the client side with JS
asyncLoop(sequenceData, (i, next) => {
  conversion({
    html:
    `<link rel="stylesheet" href="${__dirname}/pdfStylesheet.css">
    <div id="hidden-data">${JSON.stringify(i)}</div>
    <div id="container">
      <canvas id="canvas"></canvas>
    </div>
    <script type="text/javascript" src="${__dirname}/drawing.js"></script>`,
    waitForJS: true,
    paperSize: {
      width: '1748px',
      height: '2480px',
      margin: '100px'
    },
    format: {
      quality: 100
    },
    viewportSize: {
      width: 1528,
      height: 2240
    }
  },
  (err, pdf) => {
    var output = fs.createWriteStream(`${__dirname}/outputs/output-${i.index}.pdf`);
    if (err) {
      next(err);
      return;
    }
    pdf.stream.pipe(output);
    conversion.kill();
    console.log(`printed ${__dirname}/output-${i.index}.pdf`)
    next();
  });
}, err => {
  if (err)
  {
    console.error('Error: ' + err.message);
    return;
  } else {
    console.log('Finished creating pdfs!');
  }
});
