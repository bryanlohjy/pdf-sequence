const canvas = document.getElementById('canvas');
const container = document.getElementById('container')
const dataEl = document.getElementById('hidden-data');
const pdfData = dataEl.innerText.length > 0 ? JSON.parse(dataEl.innerText) : null;
const ctx = canvas.getContext('2d');

// Resize canvas to fit container ================
canvas.width = container.clientWidth;
canvas.height = container.clientHeight;

// Draw functions ================================
ctx.fillRect(canvas.width/2 - pdfData.width/2, canvas.height/2 - pdfData.height/2, pdfData.width, pdfData.height);


// print pdf =====================================
setTimeout(function() {
    window.PHANTOM_HTML_TO_PDF_READY = true; //this will start the pdf printing
}, 500);
